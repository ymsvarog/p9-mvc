<?php
    class UsersController
    {
        private $connetion;

        public function __construct(){
            $this->connection = $db->get_connection();
        }

        public function index(){
            include_once 'app/Models/UserModel.php';

            $users = (new User()::all($this->connection));

            include_once 'views/users.php';
        }

        public function add_form(){
            include_once 'views/users.php';
        }

        public function add(){
            include_once 'app/Models/UsersModel.php';

            $name = filter_input(INPUT_POST, 'name', FILER_SANITIZE_FULL_SPECIAL_CHARS);
            $email = filter_input(INPUT_POST, 'email', FILER_SANITIZE_FULL_SPECIAL_CHARS);
            $gender = filter_input(INPUT_POST, 'gender', FILER_SANITIZE_FULL_SPECIAL_CHARS);

            if(trim($name) !== "" && trim($email) !== "" && trim($gender) !== ""){
                $user = new User($name, $email, $gender);
                $user->add($this->connection);
            }

            header('Location: ?controller=users');
        }
    }
?>