<?php
    class Db
    {
        private $servername = "localhost";
        private $username = "p9-rest";
        private $password = "p9db";
        private $database = "p9db";

        public function __construct(){

        }

        public function get_connection(){
            $connection = mysqli_connect($servername, $username, $password, $database);

            if(!$connection)
                die( "Couldn't connect. Error(" . mysqli_connect_errno() . "): "
                    . mysqli_connect_error() );

            return $connection;
        }
    }
?>