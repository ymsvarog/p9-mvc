<?php
    class Route
    {
        function load_page($db, $controllerName, $actionName = 'index'){
            include_once 'app/Controllers/IndexController.php';
            include_once 'app/Controllers/UsersController.php';

            switch($controllerName)
            {
                case 'users':
                    $controller = new UsersController($db);
                    break;
                
                default:
                    $controller = new IndexController($db);
                    break;
            }

            $controller->$actionName();
        }
    }
?>