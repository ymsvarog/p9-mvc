<DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=1.0, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta http-quiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="assets/css/materialize.min.css" />
    <style>
        body{
            padding-top: 3rem;
        }
        .container{
            width: 400px;
        }
    </style>
</head>
<body>
    <div clas="container">
        <div class="row">
            <table>
                <?php foreach($users as $usr): ?>
                    <tr>
                        <td><?=$usr['name']?></td>
                        <td><?=$usr['email']?></td>
                        <td><?=$usr['gender']?></td>
                        <td><img src='<?=$usr['path']?>' /></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <a class="btn" href="?controller=user&action=add_form">add new user</a>
        <a class="btn" href="?controller=index">return back</a>
    </div>
</body>
</html>