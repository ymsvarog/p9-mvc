<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta http-quiv="X-UA-Compatiple" content="ie=edge" />
    <link rel="stylesheet"  href="assets/css/materialize.min.css" />
    <style>
        body{
            padding-top: 3rem;
        }
        .container{
            width: 400px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h3>Control panel</h3>
        <form action="auth.php" method="POST">
            <div class="row">
                <div class="field">
                    <label>Email: <input type="email" name="email" /></label>
                </div>
            <div>
            <div class="row">
                <div class="field">
                    <label>Password: <input type="password" name="password" /><br /></label>
                </div>
            </div>
            <input type="submit" class="btn" value="login" />
        </form>
        <div>
            <a href="?controller=users">List of all users</a>
        </div>
    </div>
</body>
</html>