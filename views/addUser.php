<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-size=1.0, maximum-size=1.0" />
    <meta http-quiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="assets/css/materialize.min.css" />
    <style>
        body{
            padding-top: 3rem;
        }
        .container{
            width: 400px;
        }
    </style>
</head>
<body>
    <div class="containter">
        <h3>Add new user</h3>
        <form action="?controller=users&action=add" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="field">
                    <label>Name: <input type="text" name="name" /><label>
                </div>
            </div>
            <div class="row">
                <div class="field">
                    <label>Email: <input type="email" name="email" /><label>
                </div>
            </div>
            <div class="row">
                <div class="field">
                    <input class="with-gap" type="radio" name="gender" value="female"/>
                    <span>Male</span>
                </div>
            </div>
            <div class="row">
                <div class="field">
                    <input class="with-gap" type="radio" name="gender" value="male"/>
                    <span>Male</span>
                </div>
            </div>
            <div class="row">
                <div class="file-field input-field">
                    <div class="btn">
                        <span>Photo</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" />
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" />    
                    </div>
                </div>
            </div>
            <input type="submit" class="btn" value="add" />
        </form>
    </div>
</body>
</html>